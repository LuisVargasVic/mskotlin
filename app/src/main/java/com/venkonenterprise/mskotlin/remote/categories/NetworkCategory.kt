package com.venkonenterprise.mskotlin.remote.categories

/**
 * Created by Luis Vargas on 2019-07-24.
 */

data class NetworkCategory(
    val id: Int,
    val name: String
)