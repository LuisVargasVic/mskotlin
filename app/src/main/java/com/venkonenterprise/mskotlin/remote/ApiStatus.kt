package com.venkonenterprise.mskotlin.remote

enum class ApiStatus {
    LOADING,
    STOP,
    APPENDING,
    MAX,
    ERROR
}