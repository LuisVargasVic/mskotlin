package com.venkonenterprise.mskotlin.domain.series

/**
 * Created by Luis Vargas on 2019-07-24.
 */

data class CategorySerie(
    val id: Int,
    val name: String
)